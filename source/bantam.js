// The core functionality of the BantamJS library
(function(global){
  var c = {};
  
  // Initialize the library
  c.init = function() {

    // Add the prototype version of get
    Element.prototype.get = function(identifier) {
      if (identifier[0] === '#') {
        return this.querySelector(identifier);
      } else {
        return this.querySelectorAll(identifier);
      }
    };

    // Add the prototype class add
    Element.prototype.add = function(className) {
      this.classList.add(className);
    };

    // Add the prototype class remove
    Element.prototype.erase = function(className) {
      while (this.classList.contains(className)) {
        this.classList.remove(className);
      }
    };

    // Add the prototype toggle
    Element.prototype.toggle = function(className) {
      if (this.classList.contains(className)) {
        this.erase(className);
      } else {
        this.add(className);
      }
    };
  };

  // The element lookup function
  c.get = function(identifier) {
    return document.documentElement.get(identifier);
  };

  // The template rendering function
  c.render = function(template, data) {
    for (var property in data) {
      var re = new RegExp('{' + property + '}', 'g');
      template = template.replace(re, data[property]);
    }
    return template;
  }

  // The map function
  c.map = function(list, callback) {
    var result = [];
    for (var i=0; i<list.length; i++) {
      result.push(callback(list[i],i));
    }
    return result;
  }

  // The ajax function
  c.ajax = function(endpoint, settings) {
    var xhr = new XMLHttpRequest();
    
    // Instantiate the default settings
    var method = 'GET';
    var headers = {};
    var content = '';

    // Load any settings passed in the settings object
    if (settings){
      if (settings.method) {
        method = settings.method;
      }
      if (settings.headers) {
        headers = settings.headers;
      }
      if (settings.data) {
        content = settings.data;
      }
    }

    // Format the content into a string
    var cStr = content;
    if ((typeof cStr) !== 'string') {
      if (headers['Content-Type'] 
          && headers['Content-Type'] === 'application/json') {
        cStr = JSON.stringify(content);
      } else {
        cStr = '';
        for (var prop in content) {
          if (content.hasOwnProperty(prop)) {
            if (cStr.length > 0) { cStr+= '&'; }
            cStr+= encodeURI(prop + '=' + content[prop]);
          }
        }
      }
    }

    // Open the XHTML request
    xhr.open(method, endpoint);
    // Define the headers
    var contentDefined = false;
    for (var key in headers) {
      if (headers.hasOwnProperty(key)) {
        contentDefined = contentDefined || key.toLowerCase() === 'content-type';
        xhr.setRequestHeader(key, headers[key]);
      }
    }
    if (method.toUpperCase() !== 'GET' && !contentDefined) {
      xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    }

    // Set up the callbacks
    var failback = function() {
      if (settings.failure) {
        settings.failure({ status: xhr.status, data: xhr.responseText });
      }
    };
    xhr.onload = function() {
      if (xhr.status === 200) {
        var response = {};
        try {
          response = JSON.parse(xhr.responseText);
        }
        catch (err) {
          response = xhr.responseText;
        }
        if (settings.success){
          settings.success(response);
        }
      } else {
        failback();
      }
    };
    xhr.onerror = failback;
    xhr.onabort = failback;
    // Send the request
    if (method.toUpperCase() !== 'GET') {
      xhr.send(cStr);
    } else {
      xhr.send();
    }

  };

  // The scroll function
  c.scroll = function(point, duration, callback) {
    var iterations = duration/20;
    var step = (point - window.pageYOffset)/iterations;
    
    var tick = window.setInterval(
      function(){ 
        window.scrollBy(0,step); 
      }, 20);
    window.setTimeout(
      function(){ 
        window.clearInterval(tick);
        window.scrollTo(window.pageXOffset, point);
        if (callback) {
          callback();
        }
      }, duration);
  };

  global.$bt = c;
})(window);

$bt.init();
