<!DOCTYPE hmtl>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="BantamJS - Documentation" />
    <meta name="twitter:description" content="The JavaScript library for people who hate JavaScript libraries." />
    <meta name="twitter:image" content="https://canderegg.gitlab.io/bantam-js/images/bantam.png" />

    <title>BantamJS - Documentation</title>

    <link rel="icon" type="image/icon" href="favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel=stylesheet>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
  </head>
  <body>
    <div class="navbar">
      <div class="float">
        <a class="nav" href="index.html">Home</a>
        <a class="nav active" href="docs.html">Docs</a>
        <a class="nav" href="gallery.html">Gallery</a>
   
        <a class="tech" href="https://gitlab.com/canderegg/bantam-js">GitLab</a>
        <a class="tech hide-mobile" href="js/bantam-2.1.min.js">v2.1</a>
        <div class="reset"></div>
      </div>
    </div>
    <div class="content float">
      <h1>BantamJS Documentation</h1>

      <ul class="contents">
        <li><a href="#installation">Installation</a></li>
        <li><a href="#changelog">Recent Updates</a></li>
        <li><a href="#library-functions">Library Functions</a>
          <ul>
            <li><a href="#get"><span class="code">$bt.get</span></a></li>
            <li><a href="#map"><span class="code">$bt.map</span></a></li>
            <li><a href="#add"><span class="code">.add</span></a></li>
            <li><a href="#erase"><span class="code">.erase</span></a></li>
            <li><a href="#toggle"><span class="code">.toggle</span></a></li>
            <li><a href="#render"><span class="code">$bt.render</span></a></li>
            <li><a href="#ajax"><span class="code">$bt.ajax</span></a></li>
            <li><a href="#scroll"><span class="code">$bt.scroll</span></a></li>
          </ul>
        </li>
      </ul>

      <h2 id="installation">Installation</h2>

      <p>The recommended way of adding BantamJS to your project is using the <a href="https://canderegg.gitlab.io/bantam-coop/" target="_blank">Bantam Coop</a> module manager. Once Coop is installed, the latest version of Bantam can be installed with the following command:</p>
<pre>
$ python coop.py add bantam +2.1
</pre>

      <p>If you don't want to use Coop, Bantam can be added to your project in two additional ways: downloaded onto your server, or linked directly from ours. We recommend you download the file onto your own server, as it provides more stability in the event that our servers are offline.</p>

     <p><strong>Downloading:</strong> Download a copy of the latest minified version of the library (linked at the top of this page). Store it on your server. Add it into any required page with the following tag.
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714720.js"></script>
</p>

     <p><strong>Direct Linking:</strong> Add BantamJS into any required page with the following tag.
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714721.js"></script>
</p>

      <h2 id="changelog">Recent Updates</h2>
    
      <ul>
        <li>Version 2.1 implemented global replace in the <span class="code">$bt.render</span> function.</li>
        <li>Version 2.0 added the <span class="code">$bt.map</span> function, and removed the non-prototype versions of <span class="code">add</span>, <span class="code">erase</span>, <span class="code">remove</span>, and <span class="code">toggle</span>. <strong>These changes are not backwards compatible.</strong></li>
        <li>Version 2.0 added the ability to query on attributes to the <span class="code">get</span> method.</li>
        <li>Version 1.2 added defaults for AJAX POST requests to set the <span class="code">Content-Type</span> header to use <span class="code">application/x-www-form-urlencoded</span> when not otherwise specified.</li>
        <li>Version 1.1 added prototype versions of the <span class="code">get</span>, <span class="code">add</span>, <span class="code">remove</span>, and <span class="code">toggle</span> methods (in addition to the original <span class="code">$bt</span> versions).</li>
        <li>As of version 1.1, the <span class="code">remove</span> method has been renamed to <span class="code">erase</span> to prevent conflicts with the existing prototype <span class="code">remove</span>. It is available both as <span class="code">$bt.erase</span> and <span class="code">$bt.remove</span> (though the latter is deprecated).</li>
      </ul>

      <h2 id="library-functions">Library Functions</h2>

      <p>When BantamJS is added to a page, it creates a new global <span class="code">$bt</span> object. Most functions in the library are available through this object. The <span class="code">get</span>, <span class="code">add</span>, <span class="code">erase</span>, and <span class="code">toggle</span> methods are written into the prototype to allow them to be chained. BantamJS implements the following methods:

      <h3 id="get"><span class="code">$bt.get</span></h3>

      <p><strong>Purpose:</strong> Returns an HTML DOM element, or array of elements, that match the specified identifier.</p>

      <p><strong>Signature:</strong> <span class="code">$bt.get(identifier)</span></p>

      <p><strong>Usage:</strong> The identifier is a string taking one of four forms. When the identifier begins with a hash sign, as in <span class="code">'#element-id'</span>, this method returns the exact HTML DOM element with the given ID. When the identifier begins with a period, as in <span class="code">'.class-name'</span>, this method returns a NodeList of all the HTML DOM elements with the given class. When the identifier is enclosed in brackets, as in <span class="code">[href]</span> or <span class="code">[href="#target"]</span>, this method returns a NodeList containing all the HTML DOM elements that have the provided attribute (former case), or that have the provided value for that attribute (latter). When the identifier does not begin with any special character, as in <span class="code">'tag'</span>, the method returns a NodeList of all the HTML DOM elements of the given tag in the page.</p>

      <p>The protype version of this method can be called on an HTML DOM element, and returns specifically those elements that are descendants of the current element. For example <span class="code">$bt.get('#navbar').get('a')</span> returns all the <span class="code">&lt;a&gt;</span> tags within the element with the id <span class="code">navbar</span>.</p>

<p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714722.js"></script>
</p>

      <h3 id="map"><span class="code">$bt.map</span></h3>

      <p><strong>Purpose:</strong> Runs the specified function over a collection of elements, returning the results or modifying them in place.</p>

      <p><strong>Signature:</strong> <span class="code">$bt.map(list, callback)</span></p>

      <p><strong>Usage:</strong> The function takes two arguments. The first, <span class="code">list</span>, is an iterable collection of data to be processed. The second, <span class="code">callback</span> is a callback function that will be called on each element within <span class="code">list</span>. The <span class="code">callback</span> function takes up to two arguments: the item from the data list (passed first), and the index in the iteration (passed second). The index is useful for modifying select list entries (e.g. highlighting every other row in a table). If the callback function returns a value, the <span class="code">map</span> function returns an array of those resulting values.</p>

<p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1769811.js"></script>
</p>

      <h3 id="add"><span class="code">.add</span></h3>

      <p><strong>Purpose:</strong> Adds the provided CSS class to the given HTML DOM element.</p>

      <p><strong>Signature:</strong> <span class="code">HTMLElement.prototype.add(className)</span></p>

      <p><strong>Usage:</strong> This method can be used to add a CSS class to the given element. It does not affect any other classes currently on the element. A prototype method, it is called on the element directly. For example <span class="code">$bt.get('#identifier').add('hidden')</span>.

      <p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714723.js"></script>
</p>

      <h3 id="erase"><span class="code">.erase</span></h3>

      <p><strong>Purpose:</strong> Removes the provided CSS class from the given HTML DOM element.</p>

      <p><strong>Signature:</strong> <span class="code">HTMLElement.prototype.erase(className)</span></p>

      <p><strong>Usage:</strong> This method can be used to remove a CSS class from the given element; if the given class appears multiple times, all occurrences will be removed. This method does not affect any other classes currently on the element.</p>

      <p>A prototype method, it is called on the element directly. The syntax is identical to <span class="code">add</span>.</p>

      <p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714724.js"></script>
</pre>
</p>

      <h3 id="toggle"><span class="code">.toggle</span></h3>

      <p><strong>Purpose:</strong> Toggles the provided CSS class on the given HTML DOM element.</p>

      <p><strong>Signature:</strong> <span class="code">HTMLElement.prototype.toggle(element, className)</span></p>

      <p><strong>Usage:</strong> If the given class does not appear on the element, it will be added as in <span class="code">.add</span>. If the class already appears on the element, it will be removed as in <span class="code">.erase</span>. This method does not affect any other classes currently on the element.</p>

      <p>A prototype method, it is called on the element directly. The syntax is identical to <span class="code">add</span>.

      <p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714725.js"></script>
</p>

      <h3 id="render"><span class="code">$bt.render</span></h3>

      <p><strong>Purpose:</strong> Renders an HTML template by populating it with the provided data values.</p>

      <p><strong>Signature:</strong> <span class="code">$bt.render(template, data)</span></p>

      <p><strong>Usage:</strong> This function takes a <span class="code">template</span> parameter, a string containing raw HTML. This HTML may have one or more special template fields, denoted by a name surrounded by single curly braces (without spaces). The <span class="code">data</span> parameter is an object with properties corresponding to these template fields. For example, the template field <span class="code">{firstName}</span> will be replaced with the value in <span class="code">data.firstName</span>.</p>

      <p class="warning"><strong>Note:</strong> The performance of the render function scales linearly with the number of properties in the <span class="code">data</span> object, regardless of whether or not they appear in the template. It is inadvisable to pass data parameters with many properties beyond those needed to render the template.</p>

      <p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714726.js"></script>
</p>

      <h3 id="ajax"><span class="code">$bt.ajax</span></h3>

      <p><strong>Purpose:</strong> Makes an asynchronous AJAX call back to the server.</p>

      <p><strong>Signature:</strong> <span class="code">$bt.ajax(endpoint, settings)</span></p>

      <p><strong>Usage:</strong> The <span class="code">endpoint</span> parameter is a string containing the URL of the endpoint we are making this call to. The <span class="code">settings</span> parameter is an object containing any settings passed to this AJAX call. These may include the following:
        <ul>
          <li><span class="code">method</span> The HTTP method to use when making this call. It will be <span class="code">GET</span> by default.</li>
          <li><span class="code">headers</span> Is an object in which the string properties and their values denote any HTTP headers that need to be passed with this request. For example <span class="code">headers['Accept'] = '*/*'</span> will include the HTTP header <span class="code">Accept: */*</span> with the AJAX request.</li>
          <li><span class="code">data</span> The content to include with this request (if this is a POST request). This may be either a string or an object. If this is a string, it will be passed as is. If this is an object, and the <span class="code">headers</span> object includes a header for <span class="code">Content-Type: application/json</span>, the method will attempt to encode the <span class="code">data</span> object as JSON (note this header check is case-sensitive). If no JSON content-type header is found, the method will attempt to encode the properties of the <span class="code">data</span> object as HTML form data.</li>
          <li><span class="code">success</span> The callback function to use if this call is successful. This function will be passed a <span class="code">data</span> parameter with the contents of any response from the server.</li>
          <li><span class="code">failure</span> The callback function to use if this call is unsuccessful. This function will be passed a <span class"code">data</span> parameter containing both the HTTP status code and any error message returned by the server.</li>
        </ul>
      </p>

      <p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714727.js"></script>
</p>

      <h3 id="scroll"><span class="code">$bt.scroll</span></h3>

      <p><strong>Purpose:</strong> Scrolls the top of the browser window to the given offset (in pixels, from the top of the page).</p>

      <p><strong>Signature:</strong> <span class="code">$bt.get(offset, duration, callback)</span></p>

      <p><strong>Usage:</strong> The <span class="code">offset</span> parameter is a distance from the top of the page, in pixels. This is the point to which the top of the browser window will be scrolled. The <span class="code">duration</span> parameter is the duration of the animation, in milliseconds. The optional <span class="code">callback</span> parameter is a function that will be executed when the scrolling is complete.</p>

      <p><strong>Examples:</strong>
<script src="https://gitlab.com/canderegg/bantam-js/snippets/1714728.js"></script>
</p>

    </div>
    <div class="footer">
      Copyright &copy; 2018 Caspar Anderegg
      <br><a href="https://gitlab.com/canderegg/bantam-js/blob/master/LICENSE">License</a> | <a href="tests.html">Tests</a> | <a href="https://gitlab.com/canderegg/bantam-js/blob/master/README.md">README</a>
    </div>
    <script src="js/bantam-2.1.min.js"></script>
    <script type="text/javascript">
      var links = $bt.get('.contents')[0].get('a');
      for (var i=0; i<links.length; i++) {
        links[i].onclick = function() {
          $bt.scroll($bt.get(this.hash).offsetTop - 80, 300);
           return false;
        };
      } 
    </script>
  </body>
</html>
