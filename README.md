# BantamJS

> "Perfection is achieved, not when there is nothing left to add, but when there is nothing left to take away."  
> -- Antoine de Saint-Exupery

> Components consume services; that is, you can inject a service into a component, giving the component access to that service class. To define a class as a service in Angular, use the @Injectable decorator to provide the metadata that allows Angular to inject it into a component as a dependency.  
> -- a passage from the Angular docs

## What is BantamJS?

I hate front-end development with the passion of a dying star. I didn't used to; I spent years doing freelance web development in the early two thousands, back when web development meant HTML and CSS (with maybe a smattering of JavaScript or PHP forms thrown in). I enjoyed it.

Somwhere along the line I made the transition to application and library development, and was away from web development for over a decade. When I came back something horrible had happened. Front-end development (web development wasn't a thing anymore) had apparently been taken over by crazy people.

JavaScript didn't have functions and objects anymore, it now had services and providers and proxy template factories. You can't just link a file in your HTML - that would make too much sense. Now you have to spend a week and a half installing frameworks and toolchains and package managers. One project I worked on required me to install three (three!) separate package managers to get all the right dependencies. You need to know command interfaces for Node and NPM and Grunt and Gulp and Yarn and Ember and Lint and _what the fuck is even happening, now I'm just listing nouns._

So I did what, apparently, everybody else did: I wrote my own JavaScript library. Unlike all those other people though, I did it right.

Enter BantamJS, the JavaScript library for people who hate JavaScript libraries. Here are some benefits:

- You don't have to install anything, or compile it, or minify it. You don't even have to host it if you don't want to. Just link the script and you're good to go.
- It's lightweight. Let's be real here, 99% of the crap in your standard JavaScript library is useless bloat. How many functions from jQuery do you use? Three? You're including a hundred kilobytes of rubbish along with the things you actually need. All that code has to be transmitted, parsed, and executed. 
- It's easy to learn. Do you know JavaScript? Boom, you know BantamJS too. The library adds a grand total of eight functions. You'll only need to read the documentation for one of them.
- It doesn't hold your hand. Want to make a static site with some client-side content? Go for it! Want to make a single page app? Awesome! Want to have clear separation of concerns? You do you! I'm not your mommy.

Bantam isn't without its downsides though. These include:

- It doesn't hold your hand. If your wrote crap code before stumbling onto this library, you'll write crap code using it as well.
- It's lightweight. If you want complex functionality to do... well basically anything, you'll need to do that yourself. Want a router? Write one.
- You do have to actually know JavaScript. You'll be stripped of all those comforting templates and promises and service provider factory factories, and need to learn how a damned callback function works.

## Who should use BantamJS?

1. Me
2. Other people who hate JavaScript frameworks
3. Developers working on small-scope or hobby projects

## Who shouldn't use BantamJS?

1. Facebook
2. Masochists
2. People who are frequently changing massive amounts of the DOM and need optimal performance

## Can I use BanatamJS commercially?

Yes! BantamJS is distributed under the [MIT License](LICENSE).

## This sounds amazing, how do I get started?

Check out the [documentation here](https://canderegg.gitlab.io/bantam-js/docs.html).
